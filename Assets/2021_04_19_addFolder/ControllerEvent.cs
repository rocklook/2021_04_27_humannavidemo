﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerEvent : MonoBehaviour
{
    public GameObject controller;
    private bool mode;
    // Start is called before the first frame update
    void Start()
    {
        this.mode = controller.activeSelf;
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.Two)&& this.mode)
        {
            this.controller.SetActive(false);
            this.mode = false;          
        }
        else if(OVRInput.GetDown(OVRInput.Button.Two) && !this.mode)
        {
            this.controller.SetActive(true);
            this.mode = true;
        }
    }
}
