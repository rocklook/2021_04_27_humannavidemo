﻿using UnityEngine;
using UnityEngine.EventSystems;
using SIGVerse.RosBridge;
using SIGVerse.Common;

namespace SIGVerse.Competition.HumanNavigation
{
	public interface IRosHumanNaviStringSendHandler : IEventSystemHandler
	{
		void OnSendRosStringMessage(string message);
	}

	public class HumanNaviPubString : RosPubMessage<RosBridge.std_msgs.String>, IRosHumanNaviStringSendHandler
	{
		public override void Clear()
		{
		}

		public override void Close()
		{
			if (this.webSocketConnection != null)
			{
				this.webSocketConnection.Unadvertise(this.publisher);
			}

			base.Close();
		}

		public void OnSendRosStringMessage(string message)
		{
			SIGVerseLogger.Info("Sending message : " + message);

			RosBridge.std_msgs.String humanNaviString = new RosBridge.std_msgs.String();
			humanNaviString.data = message;

			this.publisher.Publish(humanNaviString);
		}
	}
}

