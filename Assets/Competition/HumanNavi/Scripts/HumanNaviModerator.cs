﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using SIGVerse.Common;
using SIGVerse.RosBridge;
using System.Threading;

namespace SIGVerse.Competition.HumanNavigation
{
	public interface IReachMaxWrongObjectGraspCountHandler : IEventSystemHandler
	{
		void OnReachMaxWrongObjectGraspCount();
	}


	public class HumanNaviModerator : MonoBehaviour, ITimeIsUpHandler, IStartTrialHandler, IGoToNextTrialHandler, IReceiveHumanNaviMsgHandler, ISendSpeechResultHandler, IReachMaxWrongObjectGraspCountHandler
	{
		private const int SendingAreYouReadyInterval = 1000;

		private const string MsgAreYouReady     = "Are_you_ready?";
		private const string MsgTaskSucceeded   = "Task_succeeded";
		private const string MsgTaskFailed      = "Task_failed";
		private const string MsgTaskFinished    = "Task_finished";
		private const string MsgGoToNextSession = "Go_to_next_session";
		private const string MsgMissionComplete = "Mission_complete";

		private const string ReasonTimeIsUp = "Time_is_up";
		private const string ReasonGiveUp   = "Give_up";
		private const string ReasonReachMaxWrongObjectGraspCount = "Reach_max_wrong_object_grasp_count";

		private const string MsgIamReady        = "I_am_ready";
		private const string MsgGetAvatarStatus = "Get_avatar_status";
		private const string MsgGetObjectStatus = "Get_object_status";
		private const string MsgGetSpeechState  = "Get_speech_state";
		private const string MsgGiveUp          = "Give_up";

		private const string MsgRequest     = "Guidance_request";
		private const string MsgSpeechState = "Speech_state";

		private const string TagNameOfGraspables = "Graspables";
		private const string TagNameOfFurniture  = "Furniture";

		private enum Step
		{
			Initialize,
			InitializePlayback,
			WaitForStart,
			SessionStart,
			WaitForIamReady,
            SendDemoInfo,
			SendTaskInfo,
			WaitForEndOfSession,
			WaitForNextSession,
		};

		//-----------------------------

		[HeaderAttribute("Score Manager")]
		public HumanNaviScoreManager scoreManager;

		[HeaderAttribute("Session Manager")]
		public HumanNaviSessionManager sessionManager;

		[HeaderAttribute("Avatar")]
		public GameObject avatar;
		public GameObject head;
		public GameObject body;
		public NewtonVR.NVRHand LeftHand;
		public NewtonVR.NVRHand rightHand;
		//public GameObject cameraRig;
		//public GameObject Ethan;
		public float heightThresholdForPoseReset = -0.5f;

		[HeaderAttribute("Panels for avatar")]
		public GameObject noticePanelForAvatar;
		public UnityEngine.UI.Text noticeTextForAvatar;

		[HeaderAttribute("Menu")]
		public Camera birdviewCamera;
		public GameObject startTrialPanel;
		public GameObject goToNextTrialPanel;
		public GameObject recordingModeSelectPanel; // for demo

		[HeaderAttribute("Scenario Logger")]
		public GameObject playbackManager;

		//-----------------------------

		private Vector3 initialAvatarPosition;
		private Vector3 initialAvatarRotation;

		private GameObject mainMenu;
		private PanelMainController panelMainController;

		private SIGVerse.RosBridge.human_navigation.HumanNaviTaskInfo taskInfoForRobot;
		private SIGVerse.Competition.HumanNavigation.TaskInfo currentTaskInfo;

        private SIGVerse.RosBridge.human_navigation.HumanNaviDemo demoForRobot;

		private Step step;

		private float waitingTime;

		private bool isCompetitionStarted = false;
		private bool isDuringSession = false;

		private Dictionary<string, bool> receivedMessageMap;
		private bool isTargetAlreadyGrasped;
		private bool goNextSession = false;
		private bool isAllTaskFinished = false;
		private string interruptedReason;

		private StepTimer stepTimer;

		private HumanNaviPlaybackRecorder playbackRecorder;

		private Vector3 initialTargetObjectPosition;
		private Vector3 initialDestinationPosition;

		private string objectIdInLeftHand;
		private string objectIdInRightHand;

		private string objectIdInLeftHandPreviousFrame;
		private string objectIdInRightHandPreviousFrame;

        //add code
        private int numberOfSendingMessage;

		private List<string> alreadyGraspedObjects;

		private int numberOfSession;

		//private int countWrongObjectsGrasp;

		//-----------------------------

		private IRosConnection[] rosConnections;

		//----------------------------- for demo

		private string playbackFileNameTokenForEnvironmentID = "___";
		private string subjectID = "";

		private bool recordingWasAccepted = false;

		void Awake()
		{
			try
			{
				// Playback system
				this.playbackRecorder = this.playbackManager.GetComponent<HumanNaviPlaybackRecorder>();

				// Avatar 
				this.initialAvatarPosition = this.avatar.transform.position;
				this.initialAvatarRotation = this.avatar.transform.eulerAngles;

				// GUI
				this.mainMenu = GameObject.FindGameObjectWithTag("MainMenu");
				this.panelMainController = mainMenu.GetComponent<PanelMainController>();

				this.noticePanelForAvatar.SetActive(false);
				this.noticeTextForAvatar.text = "";

				// MessageMap
				this.receivedMessageMap = new Dictionary<string, bool>();
				this.receivedMessageMap.Add(MsgIamReady, false);
				this.receivedMessageMap.Add(MsgGetAvatarStatus, false);
				this.receivedMessageMap.Add(MsgGetObjectStatus, false);
				this.receivedMessageMap.Add(MsgGetSpeechState, false);
				this.receivedMessageMap.Add(MsgGiveUp, false);

				// ROSBridge
				// (Should be read after the robot is instantiated (after Awake process of SessionManager))
				this.rosConnections = SIGVerseUtils.FindObjectsOfInterface<IRosConnection>();
				SIGVerseLogger.Info("ROS connection : count=" + this.rosConnections.Length);

				// Timer
				this.stepTimer = new StepTimer();

                this.numberOfSendingMessage = 0;
                HumanNaviConfig.Instance.configInfo.instructionGenerator = "SOBITS";

            }
			catch (Exception exception)
			{
				Debug.LogError(exception);
				SIGVerseLogger.Error(exception.Message);
				SIGVerseLogger.Error(exception.StackTrace);
				this.ApplicationQuitAfter1sec();
			}
		}

		void Start()
		{
			this.step = Step.Initialize;

			//this.panelMainController.SetTeamNameText("Team: " + HumanNaviConfig.Instance.configInfo.teamName); // for competition
			this.panelMainController.SetTeamNameText("Subject ID: "); // for demo (tentative)

			this.ShowStartTaskPanel();

			this.interruptedReason = string.Empty;
		}

		//void OnDestroy()
		//{
		//	this.CloseRosConnections();
		//}

		// Update is called once per frame
		void Update()
		{
			try
			{
				if (this.isAllTaskFinished){ 
					return;
				}

				if (this.interruptedReason != string.Empty && this.step != Step.WaitForNextSession)
				{
					SIGVerseLogger.Info("Failed '" + this.interruptedReason + "'");
					this.SendPanelNotice("Failed\n" + interruptedReason.Replace('_', ' '), 100, PanelNoticeStatus.Red);
					this.TimeIsUp();
				}

				if ((OVRInput.Get(OVRInput.RawButton.LThumbstick) && OVRInput.Get(OVRInput.RawButton.X) && OVRInput.Get(OVRInput.RawButton.Y)) ||
				    (OVRInput.Get(OVRInput.RawButton.RThumbstick) && OVRInput.Get(OVRInput.RawButton.A) && OVRInput.Get(OVRInput.RawButton.B)) ||
				    (Input.GetKeyDown(KeyCode.Escape)))
				{
					this.OnGiveUp();
				}

				if (OVRInput.GetDown(OVRInput.RawButton.X) && this.isDuringSession)
				{
					if (!this.sessionManager.GetTTSRuningState())
					{
						this.SendRosHumanNaviMessage(MsgRequest, "");
					}
				}

				//if (this.avatar.transform.position.y < heightThresholdForPoseReset)
				//{
				//	this.ResetAvatarTransform();
				//}

				switch (this.step)
				{
					case Step.Initialize:
					{
						if (this.isCompetitionStarted)
						{
							this.PreProcess();
							this.step++;
						}

						break;
					}
					case Step.InitializePlayback: // Need to wait for an update to finish instantiating objects to record
					{
						this.InitializePlayback();
						this.step++;
						break;
					}
					case Step.WaitForStart:
					{
						if (this.stepTimer.IsTimePassed((int)this.step, 3000))
						{
							if (this.IsPlaybackInitialized() && this.IsConnectedToRos())
							{
								this.step++;
							}
						}

						break;
					}
					case Step.SessionStart:
					{
						this.goToNextTrialPanel.SetActive(false);
						this.recordingModeSelectPanel.SetActive(false); // for demo

						this.scoreManager.TaskStart();
						this.StartPlaybackRecord();

						if (HumanNaviConfig.Instance.configInfo.instructionGenerator == InstructionGenerator.Benoit.ToString())
						{
							this.RecordEventLog("Strategy: "+demoForRobot.strat.strategyName + ", language: " + demoForRobot.language.language);
						}
						else if (HumanNaviConfig.Instance.configInfo.instructionGenerator == InstructionGenerator.SOBITS.ToString())
						{
							this.RecordEventLog("Language: " + this.sessionManager.GetTTSLanguage());
						}

						SIGVerseLogger.Info("Session start!");
						this.RecordEventLog("Session_start");

						this.SendPanelNotice("Session start!", 100, PanelNoticeStatus.Green);
						base.StartCoroutine(this.ShowNoticeMessagePanelForAvatar("Session start!", 3.0f));

						this.isDuringSession = true;
						this.step++;

						break;
					}
					case Step.WaitForIamReady:
					{
                        if (this.receivedMessageMap[MsgIamReady])
						{
							//this.StartPlaybackRecord();
							this.step++;
							break;
						}

                        this.SendMessageAtIntervals(MsgAreYouReady, "", SendingAreYouReadyInterval);

						break;
					}
                    case Step.SendDemoInfo:
                    {
                        Debug.Log("config_value = "+ HumanNaviConfig.Instance.configInfo.instructionGenerator);
                        Debug.Log("instructionGenerator = " + InstructionGenerator.SOBITS.ToString());
                        if (HumanNaviConfig.Instance.configInfo.instructionGenerator == InstructionGenerator.Benoit.ToString())
						{
							this.SendDemoMessage(this.demoForRobot);
						}
						else if (HumanNaviConfig.Instance.configInfo.instructionGenerator == InstructionGenerator.SOBITS.ToString())
						{
							string languageStr = "";
							if (this.sessionManager.GetTTSLanguage() == "409")
							{
								languageStr = "en";
							}
							else if (this.sessionManager.GetTTSLanguage() == "411")
							{
								languageStr = "ja";
							}

							this.SendRosDemoLanguageStringMessage(languageStr);
						}
						this.step++;

                        break;
                    }
					case Step.SendTaskInfo:
					{
						this.SendRosTaskInfoMessage(this.taskInfoForRobot);

						SIGVerseLogger.Info("Waiting for end of session");

						this.step++;

						break;
					}
					case Step.WaitForEndOfSession:
					{
						// for score (grasp)
						this.JudgeGraspingObject();

						// for log (grasp)
						this.CheckGraspingStatus(this.LeftHand);
						this.CheckGraspingStatus(this.rightHand);

						// for avatar status
						this.objectIdInLeftHandPreviousFrame = this.objectIdInLeftHand; // Tentative Code
						this.objectIdInRightHandPreviousFrame = this.objectIdInRightHand; // Tentative Code
						this.objectIdInLeftHand  = this.GetGraspingObjectId(this.LeftHand);
						this.objectIdInRightHand = this.GetGraspingObjectId(this.rightHand);

						// for penalty of distance between the robot and the target/destination
						this.JudgeDistanceFromTargetObject();
						this.JudgeDistanceFromDestination();

						break;
					}
					case Step.WaitForNextSession:
					{
						if(this.goNextSession)
						{
							SIGVerseLogger.Info("Go to next session");
							this.RecordEventLog("Go_to_next_session");

							this.SendRosHumanNaviMessage(MsgGoToNextSession, "");

							this.step = Step.Initialize;
						}

						break;
					}
				}
			}
			catch (Exception exception)
			{
				Debug.LogError(exception);
				SIGVerseLogger.Error(exception.Message);
				SIGVerseLogger.Error(exception.StackTrace);
				this.ApplicationQuitAfter1sec();
			}
		}

		//-----------------------------

		private void ApplicationQuitAfter1sec()
		{
			this.CloseRosConnections();

			Thread.Sleep(1000);
			Application.Quit();
		}

		//-----------------------------

		private void PreProcess()
		{
			this.ResetFlags();

			this.subjectID = "#" + Directory.GetFiles(Application.dataPath + "/../SIGVerseConfig/HumanNavi/", "Playback-*").Length.ToString("D4");

			//this.panelMainController.SetTeamNameText("Team: " + HumanNaviConfig.Instance.configInfo.teamName);
			string subjectIDText = string.Empty;
			if (this.recordingWasAccepted)
			{
				subjectIDText = "Subject ID: " + this.subjectID;
			}
			else
			{
				subjectIDText = "Subject ID: N/A";
			}
			this.panelMainController.SetTeamNameText(subjectIDText); // for demo (tentative)

			HumanNaviConfig.Instance.InclementNumberOfTrials(HumanNaviConfig.Instance.configInfo.playbackType);

			//int countOfSessions = HumanNaviConfig.Instance.numberOfTrials;
			//if (HumanNaviConfig.Instance.configInfo.group == "B")
			//{
			//	int halfOfMaxNumberOfSessions = (int)(HumanNaviConfig.Instance.configInfo.maxNumberOfTrials / 2);
			//	if (countOfSessions > halfOfMaxNumberOfSessions)
			//	{
			//		this.numberOfSession = countOfSessions - halfOfMaxNumberOfSessions;
			//	}
			//	else
			//	{
			//		this.numberOfSession = countOfSessions + halfOfMaxNumberOfSessions;
			//	}
			//}
			//else
			//{
			//	this.numberOfSession = HumanNaviConfig.Instance.numberOfTrials;
			//}

			this.numberOfSession = HumanNaviConfig.Instance.numberOfTrials;

			//this.panelMainController.SetTrialNumberText(HumanNaviConfig.Instance.numberOfTrials);
			this.panelMainController.SetTrialNumberText(this.numberOfSession);
			SIGVerseLogger.Info("##### " + this.panelMainController.GetTrialNumberText() + " #####");

			this.panelMainController.SetTaskMessageText("");

			this.scoreManager.ResetTimeLeftText();

			//this.sessionManager.ResetEnvironment();
			this.sessionManager.ResetEnvironment(this.numberOfSession);
			this.ResetAvatarTransform();

			this.ClearRosConnections();
			this.sessionManager.ResetRobot();
			this.rosConnections = SIGVerseUtils.FindObjectsOfInterface<IRosConnection>();
			SIGVerseLogger.Info("ROS connection : count=" + this.rosConnections.Length);

			//this.currentTaskInfo = this.sessionManager.GetCurrentTaskInfo();
			this.currentTaskInfo = this.sessionManager.GetCurrentTaskInfo(this.numberOfSession);

			this.taskInfoForRobot = new SIGVerse.RosBridge.human_navigation.HumanNaviTaskInfo();
			string currentEnvironmentName = this.sessionManager.GetCurrentEnvironment().name;
			this.taskInfoForRobot.environment_id = currentEnvironmentName.Substring(0, currentEnvironmentName.Length - 3);
			this.SetObjectListToHumanNaviTaskInfo();
			this.SetFurnitureToHumanNaviTaskInfo();
			this.SetDestinationToHumanNaviTaskInfo();

			if (HumanNaviConfig.Instance.configInfo.instructionGenerator == InstructionGenerator.Benoit.ToString())
			{
				this.demoForRobot = new SIGVerse.RosBridge.human_navigation.HumanNaviDemo();
				this.SetLanguageToHumanNaviDemo();
				this.SetStrategyToHumanNaviDemo();
			}
			else if (HumanNaviConfig.Instance.configInfo.instructionGenerator == InstructionGenerator.SOBITS.ToString())
			{
			}

            this.waitingTime = 0.0f;

			this.interruptedReason = string.Empty;

			this.objectIdInLeftHand  = "";
			this.objectIdInRightHand = "";
			this.objectIdInLeftHandPreviousFrame  = ""; // Tentative Code
			this.objectIdInRightHandPreviousFrame = ""; // Tentative Code

			this.alreadyGraspedObjects = new List<string>();

			//this.countWrongObjectsGrasp = 0;

			SIGVerseLogger.Info("End of PreProcess");
		}

		private void PostProcess()
		{
			if (HumanNaviConfig.Instance.numberOfTrials == HumanNaviConfig.Instance.configInfo.maxNumberOfTrials)
			{
				//this.SendRosHumanNaviMessage(MsgMissionComplete, "");
				//this.SendPanelNotice("All sessions have been finished", 100, PanelNoticeStatus.Green);
				//base.StartCoroutine(this.ShowNoticeMessagePanelForAvatar("Mission complete", 5.0f));

				//this.CloseRosConnections();

				//this.isAllTaskFinished = true;

				// for demo (tentative)
				HumanNaviConfig.Instance.numberOfTrials = 0;
				this.SendRosHumanNaviMessage(MsgTaskFinished, "");
				SIGVerseLogger.Info("Waiting for next task");
				base.StartCoroutine(this.ShowGotoNextPanel(3.0f));
			}
			else
			{
				this.SendRosHumanNaviMessage(MsgTaskFinished, "");

				SIGVerseLogger.Info("Waiting for next task");
				base.StartCoroutine(this.ShowGotoNextPanel(3.0f));
			}

			this.StopPlaybackRecord();

			this.isDuringSession = false;
			this.interruptedReason = string.Empty;

			this.step = Step.WaitForNextSession;
		}

		private void ResetFlags()
		{
			this.receivedMessageMap[MsgIamReady] = false;
			this.isTargetAlreadyGrasped = false;
			this.goNextSession = false;
		}

		private void ResetAvatarTransform()
		{

#if ENABLE_VRIK
			this.avatar.transform.GetComponent<HumanNaviAvatarController>().StartInitializing();
			this.avatar.transform.position = this.initialAvatarPosition;
			this.avatar.transform.eulerAngles = this.initialAvatarRotation;
			this.avatar.transform.Find("ThirdPersonEthan").localPosition = Vector3.zero;
			this.avatar.transform.Find("ThirdPersonEthanWithAnimation").localPosition = Vector3.zero;
#else
			this.avatar.transform.position = this.initialAvatarPosition;
			this.avatar.transform.eulerAngles = this.initialAvatarRotation;
			this.avatar.transform.Find("OVRCameraRig").localPosition = Vector3.zero;
			this.avatar.transform.Find("OVRCameraRig").localRotation = Quaternion.identity;
			this.avatar.transform.Find("Ethan").localPosition = Vector3.zero;
			this.avatar.transform.Find("Ethan").localRotation = Quaternion.identity;
			//this.cameraRig.transform.localPosition = Vector3.zero;
			//this.cameraRig.transform.localRotation = Quaternion.identity;
			//this.Ethan.transform.localPosition = Vector3.zero;
			//this.Ethan.transform.localRotation = Quaternion.identity;
#endif
		}

		private void SetObjectListToHumanNaviTaskInfo()
		{
			// Get graspable objects
			List<GameObject> graspableObjects = GameObject.FindGameObjectsWithTag(TagNameOfGraspables).ToList<GameObject>();
			if (graspableObjects.Count == 0)
			{
				throw new Exception("Graspable object is not found.");
			}

			foreach (GameObject graspableObject in graspableObjects)
			{
				// transtrate the coordinate system of GameObject (left-handed, Z-axis:front, Y-axis:up) to ROS coodinate system (right-handed, X-axis:front, Z-axis:up)
				Vector3 positionInROS = this.ConvertCoorinateSystemUnityToROS_Position(graspableObject.transform.position);
				Quaternion orientationInROS = this.ConvertCoorinateSystemUnityToROS_Rotation(graspableObject.transform.rotation);

				if (graspableObject.name == currentTaskInfo.target)
				{
					taskInfoForRobot.target_object.name = graspableObject.name.Substring(0, graspableObject.name.Length - 3);
					taskInfoForRobot.target_object.position = positionInROS;
					taskInfoForRobot.target_object.orientation = orientationInROS;

					// for penalty
					this.initialTargetObjectPosition = graspableObject.transform.position;
				}
				else
				{
					SIGVerse.RosBridge.human_navigation.HumanNaviObjectInfo objInfo = new SIGVerse.RosBridge.human_navigation.HumanNaviObjectInfo
					{
						name = graspableObject.name.Substring(0, graspableObject.name.Length - 3),
						position = positionInROS,
						orientation = orientationInROS
					};

					taskInfoForRobot.non_target_objects.Add(objInfo);

					SIGVerseLogger.Info("Non-target object : " + objInfo.name + " " + objInfo.position + " " + objInfo.orientation);
				}
			}
			SIGVerseLogger.Info("Target object : " + taskInfoForRobot.target_object.name + " " + taskInfoForRobot.target_object.position + " " + taskInfoForRobot.target_object.orientation);

			if (taskInfoForRobot.target_object.name == "")
			{
				throw new Exception("Target object is not found.");
			}
		}

		private void SetFurnitureToHumanNaviTaskInfo()
		{
			// Get furniture
			List<GameObject> furnitureObjects = GameObject.FindGameObjectsWithTag(TagNameOfFurniture).ToList<GameObject>();
			if (furnitureObjects.Count == 0)
			{
				throw new Exception("Furniture is not found.");
			}

			foreach (GameObject furnitureObject in furnitureObjects)
			{
				// transtrate the coordinate system of GameObject (left-handed, Z-axis:front, Y-axis:up) to ROS coodinate system (right-handed, X-axis:front, Z-axis:up)
				Vector3 positionInROS = this.ConvertCoorinateSystemUnityToROS_Position(furnitureObject.transform.position);
				Quaternion orientationInROS = this.ConvertCoorinateSystemUnityToROS_Rotation(furnitureObject.transform.rotation);

				SIGVerse.RosBridge.human_navigation.HumanNaviObjectInfo objInfo = new SIGVerse.RosBridge.human_navigation.HumanNaviObjectInfo
				{
					name = furnitureObject.name.Substring(0, furnitureObject.name.Length - 3),
					position = positionInROS,
					orientation = orientationInROS
				};

				taskInfoForRobot.furniture.Add(objInfo);

				SIGVerseLogger.Info("Furniture : " + objInfo.name + " " + objInfo.position + " " + objInfo.orientation);
			}

		}

		private void SetDestinationToHumanNaviTaskInfo()
		{
			List<GameObject> destinations = GameObject.FindGameObjectsWithTag("Destination").ToList<GameObject>();
			if (destinations.Count == 0)
			{
				throw new Exception("Destination candidate is not found.");
			}

			if (!destinations.Any(obj => obj.name == this.currentTaskInfo.destination))
			{
				throw new Exception("Destination is not found.");
			}

			GameObject destination = destinations.Where(obj => obj.name == this.currentTaskInfo.destination).SingleOrDefault();

			taskInfoForRobot.destination.position = this.ConvertCoorinateSystemUnityToROS_Position(destination.transform.position);
			taskInfoForRobot.destination.orientation = this.ConvertCoorinateSystemUnityToROS_Rotation(destination.transform.rotation);
			taskInfoForRobot.destination.size = this.ConvertCoorinateSystemUnityToROS_Position(destination.GetComponent<BoxCollider>().size);
			// TODO: size parameter depends on the scale of parent object (for now, scale of all parent objects should be scale = (1,1,1))

			// for penalty
			this.initialDestinationPosition = destination.transform.position;

			SIGVerseLogger.Info("Destination : " + taskInfoForRobot.destination);

		}

		private void SetLanguageToHumanNaviDemo()
		{
			this.demoForRobot.language.language = int.Parse(this.sessionManager.GetTTSLanguage());
			//if (this.sessionManager.GetTTSLanguage().Contains("409"))
			//{
			//	demoForRobot.language.language = 409;
			//}
			//else if (this.sessionManager.GetTTSLanguage().Contains("411"))
			//{
			//	demoForRobot.language.language = 411;
			//}

			SIGVerseLogger.Info("Language : " + demoForRobot.language.language);

		}

        private void SetStrategyToHumanNaviDemo()
        {
            // HSR as reference
            if (this.sessionManager.GetStrategy() == "HSR as reference")
            {
                demoForRobot.strat.strategyName = "HSR";
                demoForRobot.strat.pointing = false;
            }
            // User as reference
            else if (this.sessionManager.GetStrategy() == "User as reference")
            {
                demoForRobot.strat.strategyName = "User";
                demoForRobot.strat.pointing = false;
            }
            // Pointing
            else if (this.sessionManager.GetStrategy() == "Pointing")
            {
                demoForRobot.strat.strategyName = "Pointing";
                demoForRobot.strat.pointing = true;
            }
            // HSR + pointing
            else if (this.sessionManager.GetStrategy() == "HSR + Pointing")
            {
                demoForRobot.strat.strategyName = "HSR + Pointing";
                demoForRobot.strat.pointing = true;
            }
            // User + pointing
            else if (this.sessionManager.GetStrategy() == "User + Pointing")
            {
                demoForRobot.strat.strategyName = "User + Pointing";
                demoForRobot.strat.pointing = true;
            }
			else
			{
				SIGVerseLogger.Error("Invalid storategy was selected");
			}

            SIGVerseLogger.Info("Strategy : " + demoForRobot.strat.strategyName);

        }

		private void SetSessionID()
		{
			HumanNaviConfig.Instance.numberOfTrials = this.sessionManager.GetSessionIDSelectorValue();
			Debug.Log(HumanNaviConfig.Instance.numberOfTrials);
		}

        private void SendMessageAtIntervals(string message, string detail, int interval_ms = 1000)
		{
			this.waitingTime += UnityEngine.Time.deltaTime;

			if (this.waitingTime > interval_ms * 0.001)
			{
				this.SendRosHumanNaviMessage(MsgAreYouReady, "");
                this.numberOfSendingMessage++;
				this.waitingTime = 0.0f;
			}
		}

		public void TimeIsUp()
		{
			this.SendRosHumanNaviMessage(MsgTaskFailed, "Time_is_up");
			this.SendPanelNotice("Time is up", 100, PanelNoticeStatus.Red);
			base.StartCoroutine(this.ShowNoticeMessagePanelForAvatar("Time is up", 3.0f));

			this.TaskFinished();
		}

		private IEnumerator ShowGotoNextPanel(float waitTime = 1.0f)
		{
			yield return new WaitForSeconds(waitTime);
			this.goToNextTrialPanel.SetActive(true);
			this.ShowNoticeMessagePanelForAvatar("Waiting for the next session to start");
			//base.StartCoroutine(this.ShowNoticeMessagePanelForAvatar("Waiting for the next session to start", 10.0f));
		}

		private void TaskFinished()
		{
			this.scoreManager.TaskEnd();
			this.PostProcess();
		}

		public void OnReceiveRosMessage(RosBridge.human_navigation.HumanNaviMsg humanNaviMsg)
		{
			if (!this.isDuringSession)
			{
				SIGVerseLogger.Warn("Illegal timing [session is not started]");
				return;
			}

			if (this.receivedMessageMap.ContainsKey(humanNaviMsg.message))
			{
				if (humanNaviMsg.message == MsgIamReady)
				{
					if(this.step != Step.WaitForIamReady)
					{
						SIGVerseLogger.Warn("Illegal timing [message : " + humanNaviMsg.message + ", step:" + this.step + "]");
						return;
					}
				}

				if (humanNaviMsg.message == MsgGetAvatarStatus)
				{
					this.SendRosAvatarStatusMessage();
				}

				if (humanNaviMsg.message == MsgGetObjectStatus)
				{
					this.SendRosObjectStatusMessage();
				}

				if (humanNaviMsg.message == MsgGetSpeechState)
				{
					this.SendRosHumanNaviMessage(MsgSpeechState, this.sessionManager.GetSeechRunStateMsgString());
				}

				if (humanNaviMsg.message == MsgGiveUp)
				{
					this.OnGiveUp();
				}

				this.receivedMessageMap[humanNaviMsg.message] = true;
			}
			else
			{
				SIGVerseLogger.Warn("Received Illegal message [message: " + humanNaviMsg.message +"]");
			}
		}

		public void OnSendSpeechResult(string speechResult)
		{
			this.SendRosHumanNaviMessage("Speech_result", speechResult);
		}

		private Vector3 ConvertCoorinateSystemUnityToROS_Position(Vector3 unityPosition)
		{
			return new Vector3(unityPosition.z, -unityPosition.x, unityPosition.y);
		}
		private Quaternion ConvertCoorinateSystemUnityToROS_Rotation(Quaternion unityQuaternion)
		{
			return new Quaternion(-unityQuaternion.z, unityQuaternion.x, -unityQuaternion.y, unityQuaternion.w);
		}

		private void InitializePlayback()
		{
			if (HumanNaviConfig.Instance.configInfo.playbackType == WorldPlaybackCommon.PlaybackTypeRecord)
			{
				this.playbackRecorder.SetPlaybackTargets();

				string languageStr = "";
				//if (this.demoForRobot.language.language == 409)
				if (this.sessionManager.GetTTSLanguage() == "409")
				{
					languageStr = "en";
				}
				else if (this.sessionManager.GetTTSLanguage() == "411")
				{
					languageStr = "ja";
				}

				// for demo (tentative)
				string filePath = string.Empty;
				filePath += Application.dataPath + "/../SIGVerseConfig/HumanNavi/Playback-";
				filePath += this.subjectID + "-";

				if (HumanNaviConfig.Instance.configInfo.instructionGenerator == InstructionGenerator.Benoit.ToString())
				{
					filePath += "Benoit-";
					filePath += this.demoForRobot.strat.strategyName + "-";
				}
				else if (HumanNaviConfig.Instance.configInfo.instructionGenerator == InstructionGenerator.SOBITS.ToString())
				{
					filePath += "SOBITS-";
				}

				filePath += languageStr + "-";
				filePath += DateTime.Now.ToString("yyyyMMdd-HHmmss-fff");
				filePath += this.playbackFileNameTokenForEnvironmentID;
				filePath += this.currentTaskInfo.environment;
				filePath += ".dat";
				this.playbackRecorder.Initialize(filePath);

				//this.playbackRecorder.Initialize(this.numberOfSession); // for competition
			}
		}

		private bool IsPlaybackInitialized()
		{
			if (HumanNaviConfig.Instance.configInfo.playbackType == WorldPlaybackCommon.PlaybackTypeRecord)
			{
				if (!this.playbackRecorder.IsInitialized()) { return false; }
			}

			return true;
		}

		public void StartPlaybackRecord()
		{
			if (HumanNaviConfig.Instance.configInfo.playbackType == WorldPlaybackCommon.PlaybackTypeRecord)
			{
				bool isStarted = this.playbackRecorder.Record();

				if (!isStarted) { SIGVerseLogger.Warn("Cannot start the world playback recording"); }
			}
		}

		public void StopPlaybackRecord()
		{
			if (HumanNaviConfig.Instance.configInfo.playbackType == WorldPlaybackCommon.PlaybackTypeRecord)
			{
				bool isStopped = this.playbackRecorder.Stop();

				if (!isStopped) { SIGVerseLogger.Warn("Cannot stop the world playback recording"); }
			}
		}

		private IEnumerator ShowNoticeMessagePanelForAvatar(string text, float waitTime = 1.0f)
		{
			this.noticeTextForAvatar.text = text;
			this.noticePanelForAvatar.SetActive(true);

			yield return new WaitForSeconds(waitTime);

			this.noticePanelForAvatar.SetActive(false);
		}

		private void ShowNoticeMessagePanelForAvatar(string text)
		{
			this.noticeTextForAvatar.text = text;
			this.noticePanelForAvatar.SetActive(true);
		}

		private void SendRosHumanNaviMessage(string message, string detail)
		{
			ExecuteEvents.Execute<IRosHumanNaviMessageSendHandler>
			(
				target: this.gameObject, 
				eventData: null, 
				functor: (reciever, eventData) => reciever.OnSendRosHumanNaviMessage(message, detail)
			);

			ExecuteEvents.Execute<IPlaybackRosMessageHandler>
			(
				target: this.playbackManager,
				eventData: null,
				functor: (reciever, eventData) => reciever.OnSendRosMessage(new SIGVerse.RosBridge.human_navigation.HumanNaviMsg(message, detail))
			);
		}

		private void SendRosAvatarStatusMessage()
		{
			RosBridge.human_navigation.HumanNaviAvatarStatus avatarStatus = new RosBridge.human_navigation.HumanNaviAvatarStatus();

			avatarStatus.head.position = ConvertCoorinateSystemUnityToROS_Position(this.head.transform.position);
			avatarStatus.head.orientation = ConvertCoorinateSystemUnityToROS_Rotation(this.head.transform.rotation);
			avatarStatus.body.position = ConvertCoorinateSystemUnityToROS_Position(this.body.transform.position);
			avatarStatus.body.orientation = ConvertCoorinateSystemUnityToROS_Rotation(this.body.transform.rotation);
			avatarStatus.left_hand.position = ConvertCoorinateSystemUnityToROS_Position(this.LeftHand.transform.position);
			avatarStatus.left_hand.orientation = ConvertCoorinateSystemUnityToROS_Rotation(this.LeftHand.transform.rotation);
			avatarStatus.right_hand.position = ConvertCoorinateSystemUnityToROS_Position(this.rightHand.transform.position);
			avatarStatus.right_hand.orientation = ConvertCoorinateSystemUnityToROS_Rotation(this.rightHand.transform.rotation);
			avatarStatus.object_in_left_hand = this.objectIdInLeftHand == "" ? "" : this.objectIdInLeftHand.Substring(0, this.objectIdInLeftHand.Length - 3);
			avatarStatus.object_in_right_hand = this.objectIdInRightHand == "" ? "" : this.objectIdInRightHand.Substring(0, this.objectIdInRightHand.Length - 3);
			avatarStatus.is_target_object_in_left_hand = this.IsTargetObject(this.objectIdInLeftHand);
			avatarStatus.is_target_object_in_right_hand = this.IsTargetObject(this.objectIdInRightHand);

			ExecuteEvents.Execute<IRosAvatarStatusSendHandler>
			(
				target: this.gameObject,
				eventData: null,
				functor: (reciever, eventData) => reciever.OnSendRosAvatarStatusMessage(avatarStatus)
			);

			ExecuteEvents.Execute<IRosAvatarStatusSendHandler>
			(
				target: this.playbackManager,
				eventData: null,
				functor: (reciever, eventData) => reciever.OnSendRosAvatarStatusMessage(avatarStatus)
			);
		}

		private void SendRosObjectStatusMessage()
		{
			RosBridge.human_navigation.HumanNaviObjectStatus objectStatus = new RosBridge.human_navigation.HumanNaviObjectStatus();

			List<GameObject> graspableObjects = GameObject.FindGameObjectsWithTag(TagNameOfGraspables).ToList<GameObject>();

			foreach (GameObject graspableObject in graspableObjects)
			{
				Vector3 positionInROS = this.ConvertCoorinateSystemUnityToROS_Position(graspableObject.transform.position);
				Quaternion orientationInROS = this.ConvertCoorinateSystemUnityToROS_Rotation(graspableObject.transform.rotation);

				if (graspableObject.name == currentTaskInfo.target)
				{
					objectStatus.target_object.name = graspableObject.name.Substring(0, graspableObject.name.Length - 3);
					objectStatus.target_object.position = positionInROS;
					objectStatus.target_object.orientation = orientationInROS;
				}
				else
				{
					SIGVerse.RosBridge.human_navigation.HumanNaviObjectInfo objInfo = new SIGVerse.RosBridge.human_navigation.HumanNaviObjectInfo
					{
						name = graspableObject.name.Substring(0, graspableObject.name.Length - 3),
						position = positionInROS,
						orientation = orientationInROS
					};

					objectStatus.non_target_objects.Add(objInfo);
				}
			}

			ExecuteEvents.Execute<IRosObjectStatusSendHandler>
			(
				target: this.gameObject,
				eventData: null,
				functor: (reciever, eventData) => reciever.OnSendRosObjectStatusMessage(objectStatus)
			);

			ExecuteEvents.Execute<IRosObjectStatusSendHandler>
			(
				target: this.playbackManager,
				eventData: null,
				functor: (reciever, eventData) => reciever.OnSendRosObjectStatusMessage(objectStatus)
			);
		}

		private void SendRosTaskInfoMessage(RosBridge.human_navigation.HumanNaviTaskInfo taskInfo)
		{
			ExecuteEvents.Execute<IRosTaskInfoSendHandler>
			(
				target: this.gameObject,
				eventData: null,
				functor: (reciever, eventData) => reciever.OnSendRosTaskInfoMessage(taskInfo)
			);

			ExecuteEvents.Execute<IRosTaskInfoSendHandler>
			(
				target: this.playbackManager,
				eventData: null,
				functor: (reciever, eventData) => reciever.OnSendRosTaskInfoMessage(taskInfo)
			);
		}

		private void SendDemoMessage(RosBridge.human_navigation.HumanNaviDemo demo)
		{
			ExecuteEvents.Execute<IRosDemoSendHandler>
			(
				target: this.gameObject,
				eventData: null,
				functor: (reciever, eventData) => reciever.OnSendRosDemoMessage(demo)
			);

			ExecuteEvents.Execute<IRosDemoSendHandler>
			(
				target: this.playbackManager,
				eventData: null,
				functor: (reciever, eventData) => reciever.OnSendRosDemoMessage(demo)
			);
		}

		private void SendRosDemoLanguageStringMessage(string message)
		{
			ExecuteEvents.Execute<IRosHumanNaviStringSendHandler>
			(
				target: this.gameObject, // TODO: if the object has multipe publishers with the same ROSMsg type, publishers send also non-taget messages
				eventData: null,
				functor: (reciever, eventData) => reciever.OnSendRosStringMessage(message)
			);

			ExecuteEvents.Execute<IRosHumanNaviStringSendHandler>
			(
				target: this.playbackManager,
				eventData: null,
				functor: (reciever, eventData) => reciever.OnSendRosStringMessage(message)
			);
		}

		private void JudgeGraspingObject()
		{
			this.CheckGraspOfObject(this.LeftHand);
			this.CheckGraspOfObject(this.rightHand);
		}

		private void CheckGraspOfObject(NewtonVR.NVRHand hand)
		{
			if (hand.HoldButtonDown && hand.IsInteracting)
			{
				if (hand.CurrentlyInteracting.tag == TagNameOfGraspables)
				{
					if (this.IsTargetObject(hand.CurrentlyInteracting.name))
					{
						if (!this.isTargetAlreadyGrasped)
						{
							SIGVerseLogger.Info("Target object is grasped" + "\t" + this.GetElapsedTimeText());

							this.SendPanelNotice("Target object is grasped", 100, PanelNoticeStatus.Green);

							this.scoreManager.AddScore(Score.ScoreType.CorrectObjectIsGrasped);
							//this.scoreManager.AddTimeScoreOfGrasp();
							this.scoreManager.AddTimeScore();

							this.isTargetAlreadyGrasped = true;
						}

						this.RecordEventLog("Object_Is_Grasped" + "\t" + "Target_Object" + "\t" + hand.CurrentlyInteracting.name);
					}
					else
					{
						//if (!this.isTargetAlreadyGrasped)
						{
							if (!this.alreadyGraspedObjects.Contains(hand.CurrentlyInteracting.name))
							{
								SIGVerseLogger.Info("Wrong object is grasped [new]" + "\t" + this.GetElapsedTimeText() + "\t" + this.GetElapsedTimeText());

								this.SendPanelNotice("Wrong object is grasped", 100, PanelNoticeStatus.Red);

								this.scoreManager.AddScore(Score.ScoreType.IncorrectObjectIsGrasped);
								//this.scoreManager.ImposeTimePenalty(Score.TimePnaltyType.IncorrectObjectIsGrasped);

								//this.countWrongObjectsGrasp++;

								this.alreadyGraspedObjects.Add(hand.CurrentlyInteracting.name);
							}
							else
							{
								SIGVerseLogger.Info("Wrong object is grasped [already grasped]" + "\t" + this.GetElapsedTimeText() + "\t" + this.GetElapsedTimeText());
							}

							this.RecordEventLog("Object_Is_Grasped" + "\t" + "Wrong_Object" + "\t" + hand.CurrentlyInteracting.name);
						}
					}
				}
				else// if (hand.CurrentlyInteracting.tag != "Untagged")
				{
					SIGVerseLogger.Info("Object_Is_Grasped" + "\t" + "Others" + "\t" + hand.CurrentlyInteracting.name + "\t" + this.GetElapsedTimeText());
					this.RecordEventLog("Object_Is_Grasped" + "\t" + "Others" + "\t" + hand.CurrentlyInteracting.name);
				}
			}
		}

		private string GetGraspingObjectId(NewtonVR.NVRHand hand)
		{
			string graspingObject = "";

			if (hand.HoldButtonPressed)
			{
				if (hand.IsInteracting)
				{
					if (hand.CurrentlyInteracting.tag == TagNameOfGraspables)
					{
						graspingObject = hand.CurrentlyInteracting.name;
					}
				}
			}

			return graspingObject;
		}

		private bool IsTargetObject(string objectLabel)
		{
			if (objectLabel == this.currentTaskInfo.target) return true;
			else                                            return false;
		}

		private void CheckGraspingStatus(NewtonVR.NVRHand hand)
		{
			if (hand.HoldButtonDown)
			{
				SIGVerseLogger.Info("HandInteraction" + "\t" + "HoldButtonDown" + "\t" + hand.name + "\t" + this.GetElapsedTimeText());
				this.RecordEventLog("HandInteraction" + "\t" + "HoldButtonDown" + "\t" + hand.name);
			}

			if (hand.HoldButtonUp)
			{
				string objectInhand = "";
				if      (hand.IsLeft)  { objectInhand = this.objectIdInLeftHandPreviousFrame; }
				else if (hand.IsRight) { objectInhand = this.objectIdInRightHandPreviousFrame; }

				if(objectInhand != "")
				{
					SIGVerseLogger.Info("HandInteraction" + "\t" + "ReleaseObject" + "\t" + hand.name + "\t" + this.GetElapsedTimeText());
					this.RecordEventLog("HandInteraction" + "\t" + "ReleaseObject" + "\t" + hand.name + "\t" + objectInhand);
				}
				else
				{
					SIGVerseLogger.Info("HandInteraction" + "\t" + "HoldButtonUp" + "\t" + hand.name + "\t" + this.GetElapsedTimeText());
					this.RecordEventLog("HandInteraction" + "\t" + "HoldButtonUp" + "\t" + hand.name);
				}
			}
		}

		private void JudgeDistanceFromTargetObject()
		{
			if (!this.scoreManager.IsAlreadyGivenDistancePenaltyForTargetObject())
			{
				float distanceFromTargetObject = this.sessionManager.GetDistanceFromRobot(this.initialTargetObjectPosition);
				if (distanceFromTargetObject < this.scoreManager.limitDistanceFromTarget)
				{
					this.scoreManager.AddDistancePenaltyForTargetObject();
				}
			}
		}
		private void JudgeDistanceFromDestination()
		{
			if (!this.scoreManager.IsAlreadyGivenDistancePenaltyForDestination())
			{
				float distanceFromDestination = this.sessionManager.GetDistanceFromRobot(this.initialDestinationPosition);
				if (distanceFromDestination < this.scoreManager.limitDistanceFromTarget)
				{
					this.scoreManager.AddDistancePenaltyForDestination();
				}
			}
		}

		public string GetTargetObjectName()
		{
			return this.currentTaskInfo.target;
		}

		public string GetDestinationName()
		{
			return this.currentTaskInfo.destination;
		}

		public bool IsTargetAlreadyGrasped()
		{
			return this.isTargetAlreadyGrasped;
		}

		public void TargetPlacedOnDestination()
		{
			if(!this.isDuringSession)
			{
				return;
			} 

			SIGVerseLogger.Info("Target is plasced on the destination." + "\t" + this.GetElapsedTimeText());
			this.RecordEventLog("Target is plasced on the destination.");

			this.scoreManager.AddScore(Score.ScoreType.TargetObjectInDestination);
			//this.scoreManager.AddTimeScoreOfPlacement();
			this.scoreManager.AddTimeScore();

			this.SendRosHumanNaviMessage(MsgTaskSucceeded, "");
			this.SendPanelNotice("Task succeeded", 100, PanelNoticeStatus.Green);
			base.StartCoroutine(this.ShowNoticeMessagePanelForAvatar("Task succeeded", 10.0f));

			this.TaskFinished();
		}

		public void OnTimeIsUp()
		{
			this.interruptedReason = HumanNaviModerator.ReasonTimeIsUp;
		}

		public void OnGiveUp()
		{
			if (this.isDuringSession)
			{
				this.interruptedReason = HumanNaviModerator.ReasonGiveUp;

				string strGiveup = "Give_up";
				this.SendRosHumanNaviMessage(MsgTaskFailed, strGiveup);
				this.SendPanelNotice("Give up", 100, PanelNoticeStatus.Red);
				base.StartCoroutine(this.ShowNoticeMessagePanelForAvatar(strGiveup, 3.0f));

				this.panelMainController.giveUpPanel.SetActive(false);


				this.TaskFinished();
			}
			else
			{
				SIGVerseLogger.Warn("It is a timing not allowed to give up.");
			}
		}

		public void OnReachMaxWrongObjectGraspCount()
		{
			this.interruptedReason = HumanNaviModerator.ReasonReachMaxWrongObjectGraspCount;

			string strReason = "Reach_max_wrong_object_grasp_count";
			this.SendRosHumanNaviMessage(MsgTaskFailed, strReason);
			this.SendPanelNotice("Reach max wrong object grasp count", 100, PanelNoticeStatus.Red);
			base.StartCoroutine(this.ShowNoticeMessagePanelForAvatar("Reach max wrong object grasp count", 3.0f));

			this.TaskFinished();
		}

		public void OnStartTrial()
		{
			//SIGVerseLogger.Info("Task start!");
			////this.RecordEventLog("Task_start");

			//this.startTrialPanel.SetActive(false);
			//this.isCompetitionStarted = true;

			this.startTrialPanel.SetActive(false);

			// for demo
			//if (this.isDemoMode)
			//{
				this.ShowRecordingModeSelectPanel();
			//}
			//else
			//{
			//	this.StartSession();
			//}

			this.SetSessionID(); // for restarting demo from an arbitrary session
		}

		private void StartSession()
		{
			SIGVerseLogger.Info("Task start!");

			this.isCompetitionStarted = true;
		}

		public void OnGoToNextTrial()
		{
			this.goToNextTrialPanel.SetActive(false);

			this.ShowRecordingModeSelectPanel(); // for demo

			//this.goNextSession = true;
		}

		public void GoToNextSession()
		{
			this.isCompetitionStarted = true;

			this.goNextSession = true;
		}

		public void OnAllowToRecord()
		{
			this.recordingModeSelectPanel.SetActive(false);
			HumanNaviConfig.Instance.configInfo.playbackType = WorldPlaybackCommon.PlaybackTypeRecord;

			if (!this.isCompetitionStarted)
			{
				this.StartSession();
			}
			else
			{
				this.GoToNextSession();
			}

			this.recordingWasAccepted = true;
			Debug.Log("record");
		}

		public void OnDoNotAllowToRecord()
		{
			this.recordingModeSelectPanel.SetActive(false);
			HumanNaviConfig.Instance.configInfo.playbackType = WorldPlaybackCommon.PlaybackTypeNone;

			if (!this.isCompetitionStarted)
			{
				this.StartSession();
			}
			else
			{
				this.GoToNextSession();
			}

			this.recordingWasAccepted = false;
			Debug.Log("do not record");
		}


		public void ShowStartTaskPanel()
		{
			this.startTrialPanel.SetActive(true);
		}

		public void ShowRecordingModeSelectPanel()
		{
			this.recordingModeSelectPanel.SetActive(true);
		}

		private void SendPanelNotice(string message, int fontSize, Color color)
		{
			PanelNoticeStatus noticeStatus = new PanelNoticeStatus(message, fontSize, color, 3.0f);

			// For changing the notice of a panel
			ExecuteEvents.Execute<IPanelNoticeHandler>
			(
				target: this.mainMenu,
				eventData: null,
				functor: (reciever, eventData) => reciever.OnPanelNoticeChange(noticeStatus)
			);

			// For recording
			ExecuteEvents.Execute<IPanelNoticeHandler>
			(
				target: this.playbackManager,
				eventData: null,
				functor: (reciever, eventData) => reciever.OnPanelNoticeChange(noticeStatus)
			);
		}

		private void RecordEventLog(string log)
		{
			if (this.recordingWasAccepted)
			{
				// For recording
				ExecuteEvents.Execute<IRecordEventHandler>
				(
					target: this.playbackManager,
					eventData: null,
					functor: (reciever, eventData) => reciever.OnRecordEvent(log)
				);
			}
		}

		private string GetElapsedTimeText()
		{
			if (HumanNaviConfig.Instance.configInfo.playbackType == WorldPlaybackCommon.PlaybackTypeRecord)
			{
				return Math.Round(this.playbackRecorder.GetElapsedTime(), 4, MidpointRounding.AwayFromZero).ToString();
			}
			else
			{
				return Math.Round(this.scoreManager.GetElapsedTime(), 4, MidpointRounding.AwayFromZero).ToString();
			}
		}

		public bool IsConnectedToRos()
		{
			foreach (IRosConnection rosConnection in this.rosConnections)
			{
				if (!rosConnection.IsConnected())
				{
					return false;
				}
			}
			return true;
		}

		public void ClearRosConnections()
		{
			foreach (IRosConnection rosConnection in this.rosConnections)
			{
				//Debug.Log(rosConnection.ToString());
				rosConnection.Clear();
			}

			SIGVerseLogger.Info("Clear ROS connections");
		}

		public void CloseRosConnections()
		{
			foreach (IRosConnection rosConnection in this.rosConnections)
			{
				rosConnection.Close();
			}

			SIGVerseLogger.Info("Close ROS connections");
		}

		//private string UpdateTaskMessage()
		//{
		//	string taskMsg = String.Format("Wrong Objects: [{0:00}]          Number of Instruction: [{1:00}]          Collision: [{2:00}]\n", this.countWrongObjectsGrasp, ;

		//	return taskMsg;
		//}
	}
}