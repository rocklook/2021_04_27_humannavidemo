using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine.XR;

namespace SIGVerse.Competition.HumanNavigation
{
	[RequireComponent(typeof (HumanNaviPlaybackCommon))]
	public class HumanNaviPlaybackPlayer : TrialPlaybackPlayer
	{
		[HeaderAttribute("Handyman Objects")]
		public HumanNaviScoreManager scoreManager;

		[HeaderAttribute("Session Manager")]
		public HumanNaviSessionManager sessionManager;

		[HeaderAttribute("Playback Data")]
		public Dropdown playbackFiles;

		[HeaderAttribute("Virtual Hands for Playback")]
		public GameObject leftHandPrefab;
		public GameObject rightHandPrefab;
		private string objectNameOfLefthand = "EthanLeftHand(Clone)";
		private string objectNameOfRighthand = "EthanRightHand(Clone)";

		private GameObject sapi;

		private PlaybackGuidanceMessageEventController guidanceMessageController;

		private bool hasVRIK = false;

		protected override void Awake()
		{
			this.isPlay = HumanNaviConfig.Instance.configInfo.playbackType == WorldPlaybackCommon.PlaybackTypePlay;

			base.Awake();

			if (this.isPlay)
			{
				//XRSettings.enabled = false;

				Transform robot = GameObject.FindGameObjectWithTag("Robot").transform;

				//robot.Find("CompetitionScripts").gameObject.SetActive(false);
				robot.Find("RosBridgeScripts")  .gameObject.SetActive(false);

				Transform moderator = GameObject.FindGameObjectWithTag("Moderator").transform;

				moderator.GetComponent<HumanNaviModerator>().enabled = false;
				moderator.GetComponent<HumanNaviPubMessage>().enabled = false;
				moderator.GetComponent<HumanNaviSubMessage>().enabled = false;
				moderator.GetComponent<HumanNaviPubTaskInfo>().enabled = false;
				moderator.GetComponent<HumanNaviPubAvatarStatus>().enabled = false;
				moderator.GetComponent<HumanNaviPubObjectStatus>().enabled = false;

				if(HumanNaviConfig.Instance.configInfo.instructionGenerator == InstructionGenerator.Benoit.ToString())
				{
					moderator.GetComponent<HumanNaviPubDemo>().enabled = false;
				}

				robot.GetComponentInChildren<HumanNaviSubGuidanceMessage>().enabled = false;

				this.scoreManager.enabled = false;

				// Avatar
				Transform avatar = GameObject.FindGameObjectWithTag("Avatar").transform;
				avatar.GetComponentInChildren<NewtonVR.NVRPlayer>().enabled = false;

#if ENABLE_VRIK
				// Avatar (Final IK)
				if (avatar.GetComponentInChildren<RootMotion.FinalIK.VRIK>())
				{
					this.hasVRIK = true;
				}

				if (this.hasVRIK)
				{
					avatar.transform.Find("ThirdPersonEthanWithAnimation").gameObject.SetActive(false);
					avatar.GetComponentInChildren<RootMotion.FinalIK.VRIK>().enabled = false;
				}
#endif

				if (!this.hasVRIK)
				{
					avatar.GetComponentInChildren<OVRManager>().enabled = false;
					avatar.GetComponentInChildren<OVRCameraRig>().enabled = false;

					avatar.GetComponentInChildren<Animator>().enabled = false;
					avatar.GetComponentInChildren<SIGVerse.Human.VR.SimpleHumanVRController>().enabled = false;
					avatar.GetComponentInChildren<SIGVerse.Human.IK.SimpleIK>().enabled = false;

					avatar.GetComponentInChildren<Animator>().enabled = false;
					avatar.GetComponentInChildren<SIGVerse.Human.VR.SimpleHumanVRController>().enabled = false;
					avatar.GetComponentInChildren<SIGVerse.Human.IK.SimpleIK>().enabled = false;
				}

//#if HUMAN_NAVI_PLAYBACK_AVATAR_WITH_FINAL_IK
//				// Avatar (Final IK)
//				avatar.transform.Find("ThirdPersonEthanWithAnimation").gameObject.SetActive(false);
//				avatar.GetComponentInChildren<RootMotion.FinalIK.VRIK>().enabled = false;

//				//avatar.GetComponentInChildren<NewtonVR.NVRPlayer>().enabled = false;

//#else
//				//// Avatar(Simple Oculus Ethan )
//				////avatar.GetComponentInChildren<NewtonVR.NVRPlayer>().enabled = false;
//				//avatar.GetComponentInChildren<OVRManager>().enabled = false;
//				//avatar.GetComponentInChildren<OVRCameraRig>().enabled = false;

//				//avatar.GetComponentInChildren<Animator>().enabled = false;
//				//avatar.GetComponentInChildren<SIGVerse.Human.VR.SimpleHumanVRController>().enabled = false;
//				//avatar.GetComponentInChildren<SIGVerse.Human.IK.SimpleIK>().enabled = false;

//				//avatar.GetComponentInChildren<Animator>().enabled = false;
//				//avatar.GetComponentInChildren<SIGVerse.Human.VR.SimpleHumanVRController>().enabled = false;
//				//avatar.GetComponentInChildren<SIGVerse.Human.IK.SimpleIK>().enabled = false;
//#endif

				//UnityEngine.XR.XRSettings.enabled = false;

				//foreach(GameObject graspingCandidatePosition in GameObject.FindGameObjectsWithTag("GraspingCandidatesPosition"))
				//{
				//	graspingCandidatePosition.SetActive(false);
				//}

				this.timeLimit = HumanNaviConfig.Instance.configInfo.sessionTimeLimit;

				//string directoryName = Application.dataPath + "/../SIGVerseConfig/HumanNavi/";
				//DirectoryInfo directoryInfo = new DirectoryInfo(directoryName);
				//FileInfo[] playbackFiles = directoryInfo.GetFiles("Playback-*");

				string[] playbackFileNames = Directory.GetFiles(Application.dataPath + "/../SIGVerseConfig/HumanNavi/", "Playback-*");

				foreach(string playbackFileName in playbackFileNames)
				{
					playbackFiles.options.Add(new Dropdown.OptionData() { text = System.IO.Path.GetFileName(playbackFileName) });
				}

			}
		}

		protected override void Start()
		{
			HumanNaviPlaybackCommon common = this.GetComponent<HumanNaviPlaybackCommon>();

			this.filePath = common.GetFilePath();

			//this.transformController   = new PlaybackTransformEventController  (common);  // Transform
			this.videoPlayerController = new PlaybackVideoPlayerEventController(common);  // Video Player

			this.taskInfoController = new PlaybackTaskInfoEventController(this.teamNameText, this.trialNumberText, this.timeLeftValText, this.taskMessageText);
			this.scoreController = new PlaybackScoreEventController(this.scoreText, this.totalText); // Score
			this.panelNoticeController = new PlaybackPanelNoticeEventController(this, this.mainMenu);      // Notice of a Panel
			this.collisionController = new PlaybackCollisionEventController(this.collisionEffect);       // Collision
			this.hsrCollisionController = new PlaybackHsrCollisionEventController(this.collisionEffect);    // HSR Collision

			XRSettings.enabled = false;

			Transform avatar = GameObject.FindGameObjectWithTag("Avatar").transform;

			if (avatar.Find("NVRPlayer/LeftHand/EthanLeftHand(Clone)") == null)
			{
				Instantiate(this.leftHandPrefab, avatar.Find("NVRPlayer/LeftHand")).name = this.objectNameOfLefthand;
			}
			if (avatar.Find("NVRPlayer/RightHand/EthanRightHand(Clone)") == null)
			{
				Instantiate(this.rightHandPrefab, avatar.Find("NVRPlayer/RightHand")).name = this.objectNameOfRighthand;
			}
			if (avatar.Find("NVRPlayer/LeftEyeAnchor") == null) // TODO: remove this from recording targets
			{
				GameObject leftEyeAnchor = new GameObject();
				Instantiate(leftEyeAnchor, avatar.Find("NVRPlayer")).name = "LeftEyeAnchor";
			}
			if (avatar.Find("NVRPlayer/RightEyeAnchor") == null) // TODO: remove this from recording targets
			{
				GameObject rightEyeAnchor = new GameObject();
				Instantiate(rightEyeAnchor, avatar.Find("NVRPlayer")).name = "RightEyeAnchor";
			}
			if (avatar.Find("NVRPlayer/TrackerAnchor") == null) // TODO: remove this from recording targets
			{
				GameObject trackerAnchor = new GameObject();
				trackerAnchor.name = "TrackerAnchor";
				Instantiate(trackerAnchor, avatar.Find("NVRPlayer")).name = "TrackerAnchor";
			}
			if (avatar.GetComponentInChildren<OVRCameraRig>() != null)
			{
				avatar.GetComponentInChildren<OVRCameraRig>().enabled = false;
			}


		}

		protected override void ReadData(string[] headerArray, string dataStr)
		{
			base.ReadData(headerArray, dataStr);

			this.guidanceMessageController.ReadEvents(headerArray, dataStr);
		}

		protected override void StartInitializing()
		{
			base.StartInitializing();

			this.guidanceMessageController.StartInitializingEvents();
		}

		protected override void UpdateIndexAndElapsedTime(float elapsedTime)
		{
			base.UpdateIndexAndElapsedTime(elapsedTime);

			this.guidanceMessageController.UpdateIndex(elapsedTime);
		}

		protected override void UpdateData()
		{
			base.UpdateData();

			this.guidanceMessageController.ExecutePassedAllEvents(this.elapsedTime, this.deltaTime);
		}

		protected override float GetTotalTime()
		{
			return Mathf.Max(
				base.GetTotalTime(),
				this.taskInfoController.GetTotalTime(),
				this.scoreController.GetTotalTime(),
				this.panelNoticeController.GetTotalTime(),
				this.collisionController.GetTotalTime(),
				this.hsrCollisionController.GetTotalTime(),
				this.guidanceMessageController.GetTotalTime()
			);
		}

		public override void OnReadFileButtonClick()
		{
			//this.trialNo = int.Parse(this.trialNoInputField.text);
			//string filePath = string.Format(Application.dataPath + HumanNaviPlaybackCommon.FilePathFormat, this.trialNo);
			string filePath = Application.dataPath + "/../SIGVerseConfig/HumanNavi/" + this.playbackFiles.options[this.playbackFiles.value].text;
			Debug.Log(filePath);

			//this.sessionManager.ResetEnvironment(this.trialNo);
			Match trialNoforPlayback = Regex.Match(filePath, @"Layout.......");

			Debug.Log(trialNoforPlayback.Value);
			this.sessionManager.ResetEnvironmentForPlayback(trialNoforPlayback.Value);

			HumanNaviPlaybackCommon common = this.GetComponent<HumanNaviPlaybackCommon>();

			common.SetPlaybackTargets();

			this.sapi = GameObject.FindGameObjectWithTag("Robot").GetComponentInChildren<SAPIVoiceSynthesisExternal>().gameObject;

			this.guidanceMessageController = new PlaybackGuidanceMessageEventController(this.sapi); // Guidance message
			this.transformController = new PlaybackTransformEventController(common);  // Transform

			this.Initialize(filePath);
		}
	}
}

