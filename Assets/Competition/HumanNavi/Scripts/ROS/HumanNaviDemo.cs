// Generated by gencs from human_navigation/HumanNaviDemo.msg
// DO NOT EDIT THIS FILE BY HAND!

using System;
using System.Collections;
using System.Collections.Generic;
using SIGVerse.RosBridge;
using UnityEngine;

using SIGVerse.RosBridge.human_navigation;

namespace SIGVerse.RosBridge 
{
	namespace human_navigation 
	{
		[System.Serializable]
		public class HumanNaviDemo : RosMessage
		{
			public human_navigation.HumanNaviLanguage language;
			public human_navigation.HumanNaviStrategy strat;


			public HumanNaviDemo()
			{
				this.language = new human_navigation.HumanNaviLanguage();
				this.strat = new human_navigation.HumanNaviStrategy();
			}

			public HumanNaviDemo(human_navigation.HumanNaviLanguage language, human_navigation.HumanNaviStrategy strat)
			{
				this.language = language;
				this.strat = strat;
			}

			new public static string GetMessageType()
			{
				return "human_navigation/HumanNaviDemo";
			}

			new public static string GetMD5Hash()
			{
				return "53b4546aa8b0c57b44b7c6a3441f5fd8";
			}
		} // class HumanNaviDemo
	} // namespace human_navigation
} // namespace SIGVerse.RosBridge

