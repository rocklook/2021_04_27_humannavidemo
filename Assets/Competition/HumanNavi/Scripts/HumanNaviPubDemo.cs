﻿using UnityEngine;
using UnityEngine.EventSystems;
using SIGVerse.RosBridge;
using SIGVerse.RosBridge.sensor_msgs;
using SIGVerse.Common;
using System.Collections.Generic;
using System;

namespace SIGVerse.Competition.HumanNavigation
{
    public interface IRosDemoSendHandler : IEventSystemHandler
    {
        void OnSendRosDemoMessage(RosBridge.human_navigation.HumanNaviDemo message);
    }

    public class HumanNaviPubDemo : RosPubMessage<RosBridge.human_navigation.HumanNaviDemo>, IRosDemoSendHandler
    {
        public override void Clear()
        {
        }

        public override void Close()
        {
            if (this.webSocketConnection != null)
            {
                this.webSocketConnection.Unadvertise(this.publisher);
            }

            base.Close();
        }

        public void OnSendRosDemoMessage(RosBridge.human_navigation.HumanNaviDemo message)
        {
            SIGVerseLogger.Info("Send Demo message : ");
            SIGVerseLogger.Info("Language : " + message.language.language);
            SIGVerseLogger.Info("Strategy : " + message.strat.strategyName);

            RosBridge.human_navigation.HumanNaviDemo rosMessage = new RosBridge.human_navigation.HumanNaviDemo(
                message.language,
                message.strat
                );

            this.publisher.Publish(rosMessage);
        }
    }
}

